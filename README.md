# Notes

- Architecture: MVVM, ViewModel, LiveData, DataBinding
- DI: Dagger
- Network: Retrofit, OkHttp
- Image: Picasso
- Phone/Tablet support: Two different layout files are used for different screen sizes

# Testing

- The repository, mappers and viewmodels are tested. Mockito is used for mocking dependencies.

# Improvements

- UI can be improved, some data are not shown on the product description screen
