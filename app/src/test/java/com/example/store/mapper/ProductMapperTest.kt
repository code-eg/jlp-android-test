package com.example.store.mapper

import com.example.store.model.ProductResponse
import com.example.store.util.BaseTest
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner::class)
internal class ProductMapperTest : BaseTest() {

    @Test
    fun testMap() {
        val fixtProductResponse = fixture<ProductResponse>()

        val mapper = ProductMapper()

        val product = mapper.map(fixtProductResponse)

        assertEquals(fixtProductResponse.productId, product.productId)
        assertEquals(fixtProductResponse.title, product.title)
        assertEquals(fixtProductResponse.image, product.image)
        assertEquals(fixtProductResponse.variantPriceRange.display.max, product.displayPrice)
    }
}