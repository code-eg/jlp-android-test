package com.example.store.mapper

import com.example.store.model.ProductDescriptionResponse
import com.example.store.util.BaseTest
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner::class)
internal class ProductDescriptionMapperTest : BaseTest() {

    @Test
    fun testMap() {
        val fixtProductDescriptionResponse = fixture<ProductDescriptionResponse>()

        val mapper = ProductDescriptionMapper()

        val productDescription = mapper.map(fixtProductDescriptionResponse)

        assertEquals(
            fixtProductDescriptionResponse.details.productInformation,
            productDescription.productInformation
        )
    }
}