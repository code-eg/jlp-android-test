package com.example.store.util

import com.appmattus.kotlinfixture.kotlinFixture

open class BaseTest {

    val fixture = kotlinFixture()

}