package com.example.store.repository

import com.example.store.api.ProductsApi
import com.example.store.mapper.ProductDescriptionMapper
import com.example.store.mapper.ProductMapper
import com.example.store.model.*
import com.example.store.util.BaseTest
import io.reactivex.Single
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.whenever
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner::class)
class ProductsRepositoryImplTest : BaseTest() {

    @Mock
    lateinit var mockProductsApi: ProductsApi

    @Mock
    lateinit var mockProductMapper: ProductMapper

    @Mock
    lateinit var mockProductDescriptionMapper: ProductDescriptionMapper

    @Test
    fun getProducts() {
        // prepare fixtures and mocks
        val fixtProductResponse = fixture<ProductResponse>()
        val fixtProduct = fixture<Product>()
        val fixtProductsResponse = ProductsResponse(listOf(fixtProductResponse))

        whenever(mockProductsApi.getDishwashers()).thenReturn(Single.just(fixtProductsResponse))
        whenever(mockProductMapper.map(fixtProductResponse)).thenReturn(fixtProduct)

        val repository =
            ProductsRepositoryImpl(mockProductsApi, mockProductMapper, mockProductDescriptionMapper)

        // action
        val testObserver = repository.getProducts().test()

        // asserts
        val products = testObserver.values()
        assertEquals(1, products.size)
        assertEquals(1, products.first().size)
        val product = products.first().first()
        assertEquals(fixtProduct, product)
    }

    @Test
    fun getProductDescription() {
        // prepare fixtures and mocks
        val fixtProductId = fixture<String>()
        val fixtProductDescriptionResponse = fixture<ProductDescriptionResponse>()
        val fixtProductDescription = fixture<ProductDescription>()

        whenever(mockProductsApi.getProductDescription(fixtProductId)).thenReturn(
            Single.just(
                fixtProductDescriptionResponse
            )
        )
        whenever(mockProductDescriptionMapper.map(fixtProductDescriptionResponse)).thenReturn(
            fixtProductDescription
        )

        val repository =
            ProductsRepositoryImpl(mockProductsApi, mockProductMapper, mockProductDescriptionMapper)

        // action
        val testObserver = repository.getProductDescription(fixtProductId).test()

        // asserts
        val productDescriptions = testObserver.values()
        assertEquals(1, productDescriptions.size)
        val productDescription = productDescriptions.first()
        assertEquals(fixtProductDescription, productDescription)
    }

}