package com.example.store.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.store.log.LogProvider
import com.example.store.model.Product
import com.example.store.react.SchedulerProvider
import com.example.store.repository.ProductsRepository
import com.example.store.ui.products.ProductItemViewModel
import com.example.store.ui.products.ProductItemViewModelMapper
import com.example.store.util.BaseTest
import io.reactivex.Single
import io.reactivex.schedulers.TestScheduler
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.whenever
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue


@RunWith(MockitoJUnitRunner::class)
class StoreViewModelTest : BaseTest() {

    @Mock
    lateinit var mockRepository: ProductsRepository

    @Mock
    lateinit var mockProductItemViewModelMapper: ProductItemViewModelMapper

    @Mock
    lateinit var mockLogProvider: LogProvider

    @Mock
    lateinit var mockSchedulerProvider: SchedulerProvider

    @Mock
    lateinit var mockObserver: Observer<List<ProductItemViewModel>>

    @get:Rule
    var instantExecutorRule: TestRule = InstantTaskExecutorRule()

    @Test
    fun testItemViewModels() {
        // prepare fixtures and mocks
        val fixtProduct = fixture<Product>()
        val fixtProducts = listOf(fixtProduct)
        val fixtProductItemViewModel = fixture<ProductItemViewModel>()

        whenever(mockRepository.getProducts()).thenReturn(Single.just(fixtProducts))
        whenever(mockProductItemViewModelMapper.map(fixtProduct)).thenReturn(
            fixtProductItemViewModel
        )

        val testScheduler = TestScheduler()
        whenever(mockSchedulerProvider.io()).thenReturn(testScheduler)
        whenever(mockSchedulerProvider.mainThread()).thenReturn(testScheduler)

        val viewModel = StoreViewModel(
            mockRepository,
            mockProductItemViewModelMapper,
            mockSchedulerProvider,
            mockLogProvider
        )

        viewModel.itemViewModels.observeForever(mockObserver)

        // action
        val liveData = viewModel.itemViewModels

        testScheduler.triggerActions()

        // assert
        assertNotNull(liveData)
        assertTrue(liveData.hasObservers())
        val value = liveData.value
        assertNotNull(value)
        assertEquals(1, value.size)
        val itemViewModel = value.first()
        assertEquals(fixtProductItemViewModel.productId, itemViewModel.productId)
        assertEquals(fixtProductItemViewModel.title, itemViewModel.title)
        assertEquals(fixtProductItemViewModel.imageUrl, itemViewModel.imageUrl)
        assertEquals(fixtProductItemViewModel.displayPrice, itemViewModel.displayPrice)
    }

}