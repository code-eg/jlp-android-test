package com.example.store.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.store.log.LogProvider
import com.example.store.model.ProductDescription
import com.example.store.react.SchedulerProvider
import com.example.store.repository.ProductsRepository
import com.example.store.ui.products.ProductItemViewModel
import com.example.store.util.BaseTest
import io.reactivex.Single
import io.reactivex.schedulers.TestScheduler
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.whenever
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue


@RunWith(MockitoJUnitRunner::class)
class ProductDetailsViewModelTest : BaseTest() {

    @Mock
    lateinit var mockRepository: ProductsRepository

    @Mock
    lateinit var mockLogProvider: LogProvider

    @Mock
    lateinit var mockSchedulerProvider: SchedulerProvider

    @Mock
    lateinit var mockObserver: Observer<ProductDescription>

    @get:Rule
    var instantExecutorRule: TestRule = InstantTaskExecutorRule()

    @Test
    fun testItemViewModels() {
        // prepare fixtures and mocks
        val fixtProductDescription = fixture<ProductDescription>()
        val fixtProductItemViewModel = fixture<ProductItemViewModel>()
        whenever(mockRepository.getProductDescription(fixtProductItemViewModel.productId)).thenReturn(Single.just(fixtProductDescription))

        val testScheduler = TestScheduler()
        whenever(mockSchedulerProvider.io()).thenReturn(testScheduler)
        whenever(mockSchedulerProvider.mainThread()).thenReturn(testScheduler)

        val viewModel = ProductDetailsViewModel(
            mockRepository,
            mockSchedulerProvider,
            mockLogProvider
        )

        viewModel.productInfo = fixtProductItemViewModel
        viewModel.productDescription.observeForever(mockObserver)

        // action
        val liveData = viewModel.productDescription

        testScheduler.triggerActions()

        // assert
        assertNotNull(liveData)
        assertTrue(liveData.hasObservers())
        val productDescription = liveData.value
        assertNotNull(productDescription)
        assertEquals(fixtProductDescription.productInformation, productDescription.productInformation)
    }

}