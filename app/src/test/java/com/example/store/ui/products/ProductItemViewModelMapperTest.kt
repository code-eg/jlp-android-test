package com.example.store.ui.products

import com.example.store.model.Product
import com.example.store.util.BaseTest
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner::class)
internal class ProductItemViewModelMapperTest : BaseTest() {

    @Test
    fun testMap() {
        val fixtProduct = fixture<Product>()

        val mapper = ProductItemViewModelMapper()

        val productItemViewModel = mapper.map(fixtProduct)

        assertEquals(fixtProduct.productId, productItemViewModel.productId)
        assertEquals(fixtProduct.title, productItemViewModel.title)
        assertEquals(fixtProduct.image, productItemViewModel.imageUrl)
        assertEquals(fixtProduct.displayPrice, productItemViewModel.displayPrice)
    }
}