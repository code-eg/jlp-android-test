package com.example.store.ui.products

import com.example.store.model.Product
import javax.inject.Inject

class ProductItemViewModelMapper @Inject constructor() {

    fun map(product: Product): ProductItemViewModel {
        return ProductItemViewModel(
            productId = product.productId,
            title = product.title,
            imageUrl = product.image,
            displayPrice = product.displayPrice
        )
    }

}