package com.example.store.ui.products

import android.text.Html
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.store.StoreApplication


@BindingAdapter("itemViewModels")
fun bindItemViewModels(
    recyclerView: RecyclerView,
    productItemViewModels: List<ProductItemViewModel>?
) {
    val adapter = getAdapter(recyclerView)
    adapter.updateItems(productItemViewModels)
}

@BindingAdapter("url")
fun bindUrl(
    imageView: ImageView,
    url: String
) {
    val picasso = (imageView.context.applicationContext as StoreApplication).appComponent.picasso()
    picasso.load("https:$url").fit().into(imageView)
}

@BindingAdapter("htmlText")
fun bindUrl(
    textView: TextView,
    text: String?
) {
    text?.let {
        textView.text = Html.fromHtml(it, Html.FROM_HTML_MODE_COMPACT)
    }
}

private fun getAdapter(recyclerView: RecyclerView): ProductsRecyclerViewAdapter {
    return if (recyclerView.adapter != null && recyclerView.adapter is ProductsRecyclerViewAdapter) {
        recyclerView.adapter as ProductsRecyclerViewAdapter
    } else {
        val adapter = ProductsRecyclerViewAdapter(recyclerView.context)
        recyclerView.adapter = adapter
        adapter
    }
}
