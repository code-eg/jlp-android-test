package com.example.store.ui.products

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.store.R
import com.example.store.databinding.ItemProductBinding
import com.example.store.ui.EXTRA_PRODUCT_INFO
import com.example.store.ui.ProductDetailsActivity

class ProductsRecyclerViewAdapter(private val context: Context) :
    RecyclerView.Adapter<ProductViewHolder>() {

    private var itemViewModels: List<ProductItemViewModel> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val binding: ItemProductBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_product,
            parent, false
        )

        return ProductViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.bind(itemViewModels[position])
        holder.binding.itemProduct.setOnClickListener {
            val intent = Intent(context, ProductDetailsActivity::class.java)
            intent.putExtra(EXTRA_PRODUCT_INFO, itemViewModels[position])
            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return itemViewModels.size
    }

    fun updateItems(
        productItemViewModels: List<ProductItemViewModel>?
    ) {
        this.itemViewModels = productItemViewModels ?: emptyList()
        notifyDataSetChanged()
    }

}


class ProductViewHolder(
    val binding: ItemProductBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(productItemViewModel: ProductItemViewModel) {
        binding.productItemViewModel = productItemViewModel
    }

}