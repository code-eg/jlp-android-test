package com.example.store.ui.products

import android.os.Parcelable
import kotlinx.parcelize.Parcelize


@Parcelize
class ProductItemViewModel(
    val productId: String,
    val title: String,
    val imageUrl: String,
    val displayPrice: String
) : Parcelable
