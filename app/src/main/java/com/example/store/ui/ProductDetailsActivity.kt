package com.example.store.ui

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.store.databinding.ActivityProductDetailsBinding
import com.example.store.ui.products.ProductItemViewModel
import com.example.store.viewmodel.ProductDetailsViewModel
import dagger.android.AndroidInjection
import javax.inject.Inject

const val EXTRA_PRODUCT_INFO = "product_info"

class ProductDetailsActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by viewModels<ProductDetailsViewModel> { viewModelFactory }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val binding = ActivityProductDetailsBinding.inflate(layoutInflater)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        if (intent.hasExtra(EXTRA_PRODUCT_INFO)) {
            val productInfo = intent.getParcelableExtra<ProductItemViewModel>(EXTRA_PRODUCT_INFO)
            productInfo?.let { viewModel.productInfo = it }
        }

        setContentView(binding.root)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}