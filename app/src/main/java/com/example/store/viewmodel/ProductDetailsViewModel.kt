package com.example.store.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.toLiveData
import com.example.store.log.LogProvider
import com.example.store.model.ProductDescription
import com.example.store.react.SchedulerProvider
import com.example.store.repository.ProductsRepository
import com.example.store.ui.products.ProductItemViewModel

class ProductDetailsViewModel(
    private val productsRepository: ProductsRepository,
    private val schedulerProvider: SchedulerProvider,
    private val logProvider: LogProvider
) : ViewModel() {

    lateinit var productInfo: ProductItemViewModel

    val productDescription: LiveData<ProductDescription> by lazy {
        productsRepository.getProductDescription(productInfo.productId)
            .toFlowable()
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.mainThread())
            .doOnError {
                logProvider.logError("Error getting products.", it)
            }
            .toLiveData()
    }


}