package com.example.store.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.toLiveData
import com.example.store.log.LogProvider
import com.example.store.react.SchedulerProvider
import com.example.store.repository.ProductsRepository
import com.example.store.ui.products.ProductItemViewModel
import com.example.store.ui.products.ProductItemViewModelMapper

class StoreViewModel(
    private val productsRepository: ProductsRepository,
    private val productItemViewModelMapper: ProductItemViewModelMapper,
    private val schedulerProvider: SchedulerProvider,
    private val logProvider: LogProvider
) : ViewModel() {

    val itemViewModels: LiveData<List<ProductItemViewModel>> by lazy {
        productsRepository.getProducts()
            .map {
                it.map(productItemViewModelMapper::map)
            }
            .toFlowable()
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.mainThread())
            .doOnError {
                logProvider.logError("Error getting products.", it)
            }
            .toLiveData()
    }

}