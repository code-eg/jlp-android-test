package com.example.store.api

import com.example.store.model.ProductDescriptionResponse
import com.example.store.model.ProductsResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface ProductsApi {

    @GET("/search/api/rest/v2/catalog/products/search/keyword?q=dishwasher&key=AIzaSyDD_6O5gUgC4tRW5f9kxC0_76XRC8W7_mI")
    fun getDishwashers(): Single<ProductsResponse>

    @GET("/mobile-apps/api/v1/products/{productId}")
    fun getProductDescription(@Path("productId") productId: String): Single<ProductDescriptionResponse>

}