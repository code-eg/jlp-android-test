package com.example.store.model

data class Product(

    val productId: String,
    val title: String,
    val image: String,
    val displayPrice: String

)

data class ProductDescription(

    val productInformation: String

)