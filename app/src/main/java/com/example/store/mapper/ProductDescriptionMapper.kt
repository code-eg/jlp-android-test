package com.example.store.mapper

import com.example.store.model.ProductDescription
import com.example.store.model.ProductDescriptionResponse
import javax.inject.Inject

class ProductDescriptionMapper @Inject constructor() {

    fun map(productDescriptionResponse: ProductDescriptionResponse): ProductDescription {
        return ProductDescription(
            productInformation = productDescriptionResponse.details.productInformation
        )
    }

}