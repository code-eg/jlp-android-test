package com.example.store.mapper

import com.example.store.model.Product
import com.example.store.model.ProductResponse
import javax.inject.Inject

class ProductMapper @Inject constructor() {

    fun map(productResponse: ProductResponse): Product {
        return Product(
            productId = productResponse.productId,
            title = productResponse.title,
            image = productResponse.image,
            displayPrice = productResponse.variantPriceRange.display.max
        )
    }

}