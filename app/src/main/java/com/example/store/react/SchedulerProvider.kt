package com.example.store.react

import io.reactivex.Scheduler

/**
 * Provides rx schedulers. Can be mocked in tests to use different schedulers.
 */
interface SchedulerProvider {

    fun io(): Scheduler

    fun mainThread(): Scheduler

}