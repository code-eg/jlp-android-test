package com.example.store.di

import com.example.store.ui.ProductDetailsActivity
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.multibindings.ClassKey
import dagger.multibindings.IntoMap

@Module(subcomponents = [ProductDetailsActivitySubcomponent::class])
interface ProductDetailsActivityModule {

    @Binds
    @IntoMap
    @ClassKey(ProductDetailsActivity::class)
    fun bindProductDetailsActivityInjectorFactory(factory: ProductDetailsActivitySubcomponent.Factory): AndroidInjector.Factory<*>

}