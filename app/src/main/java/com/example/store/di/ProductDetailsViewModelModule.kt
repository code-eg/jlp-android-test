package com.example.store.di

import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.example.store.log.LogProvider
import com.example.store.react.SchedulerProvider
import com.example.store.repository.ProductsRepository
import com.example.store.ui.products.ProductItemViewModelMapper
import com.example.store.viewmodel.ProductDetailsViewModel
import com.example.store.viewmodel.StoreViewModel
import dagger.Module
import dagger.Provides

@Module
class ProductDetailsViewModelModule {

    @Provides
    fun provideViewModelFactory(
        productsRepository: ProductsRepository,
        schedulerProvider: SchedulerProvider,
        logProvider: LogProvider
    ): ViewModelProvider.Factory {
        return viewModelFactory {
            initializer {
                ProductDetailsViewModel(
                    productsRepository,
                    schedulerProvider,
                    logProvider
                )
            }
        }
    }

}