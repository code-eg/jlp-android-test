package com.example.store.di

import okhttp3.Interceptor
import okhttp3.Response

/**
 * This was required, as requests with missing User-Agent returns 403 error
 */
class HeaderInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val newRequest = request.newBuilder()
            .addHeader("User-Agent", "App")
            .build()

        return chain.proceed(newRequest)
    }
}