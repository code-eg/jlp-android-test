package com.example.store.di

import com.example.store.log.LogProvider
import com.example.store.log.LogProviderImpl
import com.example.store.react.SchedulerProvider
import com.example.store.react.SchedulerProviderImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class AppModule {

    @Binds
    @Singleton
    abstract fun bindSchedulerProvider(schedulerProvider: SchedulerProviderImpl): SchedulerProvider

    @Binds
    @Singleton
    abstract fun bindLogProvider(logProvider: LogProviderImpl): LogProvider

}