package com.example.store.di

import com.example.store.api.ProductsApi
import com.example.store.mapper.ProductDescriptionMapper
import com.example.store.mapper.ProductMapper
import com.example.store.repository.ProductsRepository
import com.example.store.repository.ProductsRepositoryImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun provideProductsRepository(
        productsApi: ProductsApi,
        productMapper: ProductMapper,
        productDescriptionMapper: ProductDescriptionMapper
    ): ProductsRepository {
        return ProductsRepositoryImpl(
            productsApi,
            productMapper,
            productDescriptionMapper
        )
    }

}