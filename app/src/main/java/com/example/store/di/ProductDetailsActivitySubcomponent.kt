package com.example.store.di

import com.example.store.ui.ProductDetailsActivity
import dagger.Subcomponent
import dagger.android.AndroidInjector

@Subcomponent(
    modules = [
        ProductDetailsViewModelModule::class,
    ]
)
interface ProductDetailsActivitySubcomponent : AndroidInjector<ProductDetailsActivity> {

    @Subcomponent.Factory
    interface Factory : AndroidInjector.Factory<ProductDetailsActivity>

}