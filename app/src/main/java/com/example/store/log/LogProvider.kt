package com.example.store.log

/**
 * Interface to log an error.
 */
interface LogProvider {

    fun logError(message: String, e: Throwable)

}