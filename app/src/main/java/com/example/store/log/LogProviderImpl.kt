package com.example.store.log

import android.util.Log
import javax.inject.Inject

private const val TAG = "DishwasherStore"

class LogProviderImpl @Inject constructor() : LogProvider {

    override fun logError(message: String, e: Throwable) {
        Log.e(TAG, message, e)
    }

}