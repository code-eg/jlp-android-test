package com.example.store.repository

import com.example.store.model.Product
import com.example.store.model.ProductDescription
import io.reactivex.Single

/**
 * Interface providing API to access data.
 */
interface ProductsRepository {

    fun getProducts(): Single<List<Product>>

    fun getProductDescription(productId: String): Single<ProductDescription>

}