package com.example.store.repository

import com.example.store.api.ProductsApi
import com.example.store.mapper.ProductDescriptionMapper
import com.example.store.mapper.ProductMapper
import com.example.store.model.Product
import com.example.store.model.ProductDescription
import io.reactivex.Single
import javax.inject.Inject

class ProductsRepositoryImpl @Inject constructor(
    private val productsApi: ProductsApi,
    private val productMapper: ProductMapper,
    private val productDescriptionMapper: ProductDescriptionMapper,
) : ProductsRepository {

    override fun getProducts(): Single<List<Product>> {
        return productsApi.getDishwashers()
            .map { it.products }
            .map {
                it.map(productMapper::map)
            }
    }

    override fun getProductDescription(productId: String): Single<ProductDescription> {
        return productsApi.getProductDescription(productId)
            .map(productDescriptionMapper::map)
    }

}
